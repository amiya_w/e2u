# e2ubuntu

一个个人用的Ubuntu 19.04部署脚本


用法:
```
curl -L https://gitlab.com/amiya_w/e2u/raw/master/e2u.sh | sudo bash
```

或

**自行设置短链接替换**

Example:
```
curl -L https://deploy.amiya.cf | sudo bash //把 deploy.amiya.cf 换成你申请到的短链接
```