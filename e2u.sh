#!/bin/bash

echo "[*] Now backing up apt source list"
mv /etc/apt/sources.list /etc/apt/sources.list.bak
echo "[#] Done. File : /etc/apt/sources.list.bak"


echo "[*] Setting apt source set to Tsinghua Tuna Mirror"
echo "ZGViIGh0dHBzOi8vbWlycm9ycy50dW5hLnRzaW5naHVhLmVkdS5jbi91YnVudHUvIGRpc2NvIG1haW4gcmVzdHJpY3RlZCB1bml2ZXJzZSBtdWx0aXZlcnNlCmRlYiBodHRwczovL21pcnJvcnMudHVuYS50c2luZ2h1YS5lZHUuY24vdWJ1bnR1LyBkaXNjby11cGRhdGVzIG1haW4gcmVzdHJpY3RlZCB1bml2ZXJzZSBtdWx0aXZlcnNlCmRlYiBodHRwczovL21pcnJvcnMudHVuYS50c2luZ2h1YS5lZHUuY24vdWJ1bnR1LyBkaXNjby1iYWNrcG9ydHMgbWFpbiByZXN0cmljdGVkIHVuaXZlcnNlIG11bHRpdmVyc2UKZGViIGh0dHBzOi8vbWlycm9ycy50dW5hLnRzaW5naHVhLmVkdS5jbi91YnVudHUvIGRpc2NvLXNlY3VyaXR5IG1haW4gcmVzdHJpY3RlZCB1bml2ZXJzZSBtdWx0aXZlcnNlCmRlYiBodHRwOi8vbWlycm9ycy5hbGl5dW4uY29tL3VidW50dS8gZGlzY28gbWFpbiByZXN0cmljdGVkIHVuaXZlcnNlIG11bHRpdmVyc2UKZGViIGh0dHA6Ly9taXJyb3JzLmFsaXl1bi5jb20vdWJ1bnR1LyBkaXNjby1zZWN1cml0eSBtYWluIHJlc3RyaWN0ZWQgdW5pdmVyc2UgbXVsdGl2ZXJzZQpkZWIgaHR0cDovL21pcnJvcnMuYWxpeXVuLmNvbS91YnVudHUvIGRpc2NvLXVwZGF0ZXMgbWFpbiByZXN0cmljdGVkIHVuaXZlcnNlIG11bHRpdmVyc2UKZGViIGh0dHA6Ly9taXJyb3JzLmFsaXl1bi5jb20vdWJ1bnR1LyBkaXNjby1wcm9wb3NlZCBtYWluIHJlc3RyaWN0ZWQgdW5pdmVyc2UgbXVsdGl2ZXJzZQpkZWIgaHR0cDovL21pcnJvcnMuYWxpeXVuLmNvbS91YnVudHUvIGRpc2NvLWJhY2twb3J0cyBtYWluIHJlc3RyaWN0ZWQgdW5pdmVyc2UgbXVsdGl2ZXJzZQoK" | base64 -d >/etc/apt/sources.list
echo "[#] Done."

echo "[*] Refreshing apt cache"
apt-get -y update
echo "[#] Done."

echo "[*] Upgrading system"
apt-get -y upgrade
echo "[#] Done."

echo "[*] Insalling some basic tools."
apt-get -y install git 
echo "[#] Done."

echo "[*] Download then install deepin-wine"

git clone https://gitee.com/wszqkzqk/deepin-wine-for-ubuntu.git
sleep 1000
cd deepin-wine-for-ubuntu

cd `dirname $0`; pwd
echo '准备添加32位支持'
sudo dpkg --add-architecture i386
echo '添加成功，准备刷新apt缓存信息...'
sudo apt -y update
echo '即将开始安装...'
sudo dpkg -i *.deb
echo '安装完成，正在自动安装依赖...'
sudo apt -y install -f
echo "[#] Done."

echo "[*] Install Tencent QQ/QQ Lite/Thunder"
wget "https://mirrors.tuna.tsinghua.edu.cn/deepin/pool/non-free/d/deepin.com.qq.im/deepin.com.qq.im_8.9.19983deepin23_i386.deb" -O deepin.con.qq.im.deb
wget "https://mirrors.tuna.tsinghua.edu.cn/deepin/pool/non-free/d/deepin.com.thunderspeed/deepin.com.thunderspeed_7.10.35.366deepin18_i386.deb" -O deepin.com.thunderspeed.deb
wget "https://mirrors.tuna.tsinghua.edu.cn/deepin/pool/non-free/d/deepin.com.qq.im.light/deepin.com.qq.im.light_7.9.14308deepin8_i386.deb" -O deepin.com.qq.im.light.deb

dpkg -i deepin.con.qq.im.deb
dpkg -i deepin.com.thunderspeed.deb
dpkg -i deepin.com.qq.im.light.deb

echo "[*] Done."

